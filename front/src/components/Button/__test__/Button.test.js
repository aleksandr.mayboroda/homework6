import Button from "../index"
import { render } from "@testing-library/react"
import userEvent from "@testing-library/user-event"


describe("Testing Button component", () => {

    const func = jest.fn()
    const className = 'btn'
    const text = 'button text'
    const active = false

    test("Testing Button props", () => {
        const { getByText } = render(<Button func={func} className={className} text={text} active={active} />)
        const button = getByText(text)
    })

    test("Testing Button get className", () => {
        const { container } = render(<Button func={func} className={className} text={text} active={active} />)
        expect(container.getElementsByClassName(className).length > 0).toBe(true)
    })

    test("Testing Button change disable attribute", () => {
        const { getByText } = render(<Button func={func} className={className} text={text} active={active} />)
        expect(getByText(text)).toBeDisabled()
    })

    test("Testing button function work", () => {
        const handleClick = jest.fn()
        const { getByText } = render(<Button text={'LOL'} func={handleClick} />)
        const button = getByText(/lol/i)

        expect(handleClick).not.toHaveBeenCalled()

        userEvent.click(button)

        expect(handleClick).toHaveBeenCalled()
        
    })
})
