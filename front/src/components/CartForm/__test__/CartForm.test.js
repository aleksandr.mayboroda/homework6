import  CartForm  from "../index"
import { render, fireEvent, waitFor } from "@testing-library/react"
import { Provider } from "react-redux"
import store from "../../../store/store"

import userEvent from "@testing-library/user-event"

describe("Testing CartFrom", () => {
    test("Testing CartFrom Rendering", () => {
        render(
            <Provider store={store}>
                <CartForm />
            </Provider>
        )
    })

    // test('Testing CartForm functional', async () => {
    //     const { getByLabelText,getByText} = render(
    //         <Provider store={store}>
    //             <CartForm />
    //         </Provider>
    //     )

    //     let formSubmit = jest.fn()

    //     let firstName = getByLabelText(/first name/i)
    //     let lastName = getByLabelText(/last name/i)
    //     let age = getByLabelText(/age/i)
    //     let address = getByLabelText(/delivery address/i)
    //     let phone = getByLabelText(/phone number/i)
    //     let email = getByLabelText(/email/i)
    //     let button = getByText(/submit/i)

    //     userEvent.click(button)
    //     expect(button).not.toBeDisabled();

    //     act(() => {
    //         userEvent.type(firstName, "alex")
    //         userEvent.type(lastName, "maybee")
    //         userEvent.type(age, 23)
    //         userEvent.type(address, "address 123 jsdhjkgb jsdagkhjhakjf")
    //         userEvent.type(phone, "+380676101770")
    //         userEvent.type(email, "alex@test.ua")
    //     })

    //     userEvent.click(button)

    //     expect(formSubmit).not.toHaveBeenCalled()

    // })

    test("Fill form with correct data", async () => {
        const handleSubmit = jest.fn()
        const { getByLabelText, getByText } = render(
            <Provider store={store}>
                <CartForm handleSubmit={handleSubmit} />
            </Provider>
        )

        let firstName = getByLabelText(/first name/i)
        let lastName = getByLabelText(/last name/i)
        let age = getByLabelText(/age/i)
        let address = getByLabelText(/delivery address/i)
        let phone = getByLabelText(/phone number/i)
        let email = getByLabelText(/email/i)
        let button = getByText(/submit/i)

        userEvent.type(firstName, "alexs")
        
        userEvent.type(lastName, "maybee")
        userEvent.type(age, "23")
        userEvent.type(address, "address sa")
        userEvent.type(phone, "+38(067) 610-17-70")
        userEvent.type(email, "alex@test.ua")

        await waitFor(() => {
            expect(firstName).not.toBe("alexs")
            expect(lastName).not.toBe("")
            expect(age).not.toBe("")
            expect(address).not.toBe("")
            expect(phone).not.toBe("")
            expect(email).not.toBe("")
        })

        userEvent.click(button)
        await waitFor(() => {
            expect(handleSubmit).toHaveBeenCalledTimes(1)
        })

        // await waitFor(() =>
        //     expect(handleSubmit).toHaveBeenCalledWith({
        //         firstName: "alexs",
        //         lastName: "maybee",
        //         age: "23",
        //         address: "address",
        //         phone: "+38(067) 610-17-70",
        //         email: "alex@test.ua",
        //     })
        // )
    })
})
