import React, { PureComponent } from "react"
import './style.scss'

class ErrorBoundary extends PureComponent {
    state = {
        isError: null,
    }

    static getDerivedStateFromError(error) {
        return { isError: error }
    }

    // componentDidCatch(err,info)
    // {
    //     this.setState({
    //         isError: err
    //     })
    // }

    render() {
        if (this.state.isError) {
            return (
                <div className="boundary-block">
                    <h1 className="boundary-block__text">Something went wrong...</h1>
                </div>
            )
        }
        return this.props.children
    }
}

export default ErrorBoundary
