import React from "react"
import PropTypes from "prop-types"
import Icon from "../Icon"
import Button from '../Button'

import { useDispatch, useSelector} from "react-redux"
import { productsOperations } from "../../store/products"
import { cartSelectors } from "../../store/cart"

import useProductCartConfirm from '../../hooks/useProductCartConfirm'

const FavoritesTableItem = ({ product }) => {

    const { articul, name, imagePath, price } = product

    const dispatch = useDispatch()

    const isInCart = useSelector(cartSelectors.isProductInCart(product.articul))

    //мой хук
    const [modalHandler] = useProductCartConfirm(product)

    return (
        <tr>
            <td>
                <button
                    className="favorite-delete-btn"
                    onClick={() =>
                        dispatch(productsOperations.toggleFavorites(articul))
                    }
                >
                    <Icon type="star" filled size={"medium"} />
                </button>
            </td>
            <td>
                <Button 
                    text={isInCart ? "In cart" : "Add to cart"}
                    className={isInCart ? "btn-yellow" : "btn-water"}
                    func={() => modalHandler({...product, isInCart})}
                />
            </td>
            <td>
                <img width="200" src={imagePath} alt={name} />
            </td>
            <td>{name}</td>
            <td>{price}$</td>
        </tr>
    )
}

FavoritesTableItem.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        articul: PropTypes.string.isRequired,
        imagePath: PropTypes.string.isRequired,
        price: PropTypes.number,
    }).isRequired,
}

export default FavoritesTableItem
