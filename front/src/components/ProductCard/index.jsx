import React from "react"
import "./style.scss"

import { useDispatch, useSelector } from "react-redux"

import { productsSelectors, productsOperations } from "../../store/products"
import { cartSelectors } from "../../store/cart"

import PropTypes from "prop-types"
import Button from "../Button"
import Favorite from "../Favorite"

import useProductCartConfirm from '../../hooks/useProductCartConfirm'


const ProductCard = ({ product }) => {

    const { name, price = 0, imagePath, articul, color } = product

    const dispatch = useDispatch()

    const isInFavorites = useSelector(
        productsSelectors.isProductInFavorites(product.articul)
    ) 
    const isInCart = useSelector(cartSelectors.isProductInCart(product.articul))

    //мой хук
    const [modalHandler] = useProductCartConfirm()

    return (
        // onMouseEnter={() => console.log('hovered')}
        <div className="product">
            <div className="product__image">
                <img src={imagePath} alt={name} />
            </div>
            <div className="product__info">
                <h3 className="product__name">{name}</h3>

                <p className="product__articul">articul: {articul}</p>
                <p className="product__price">{price} $</p>
                <p className="product__color">
                    color: <span style={{ color }}>{color}</span>
                </p>
                <div className="product__actions">
                    <Favorite
                        func={() =>
                            dispatch(
                                productsOperations.toggleFavorites(articul)
                            )
                        }
                        filled={isInFavorites}
                    />
                    <Button
                        text={isInCart ? "In cart" : "Add to cart"}
                        className={isInCart ? "btn-yellow" : "btn-water"}
                        func={() => modalHandler({...product, isInCart})}
                    />
                </div>
            </div>
        </div>
    )
}

ProductCard.propTypes = {
    product: PropTypes.exact({
        name: PropTypes.string.isRequired,
        price: PropTypes.number,
        imagePath: PropTypes.string,
        articul: PropTypes.string,
        color: PropTypes.string,
    }),
}

export default ProductCard
