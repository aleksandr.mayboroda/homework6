import React from "react"
import { useDispatch } from "react-redux"

import { modalOperations } from "../store/modal"
import { cartOperations } from "../store/cart"

import Button from "../components/Button"

const useProductCartConfirm = () => {
    const dispatch = useDispatch()

    const setModalContent = (content) =>
        dispatch(modalOperations.setModalContent(content))

    const cartHandler = (content) =>
        dispatch(cartOperations.toggleCart(content))


    const modalHandler = (product) => {
        if (product === null) {
            setModalContent(null)
        } else {
            const { articul, price, isInCart } = product

            setModalContent({
                // classes: (isInCart ? 'text-white' : ''),
                // background: isInCart ? "red" : "",
                title: <h2>{isInCart ? "Warning!" : ""}</h2>,
                main: (
                    <h2 style={{ textAlign: "center" }}>
                        Are you shure you want
                        {isInCart
                            ? `to remove product from `
                            : ` to add product to `}
                        cart?
                    </h2>
                ),
                footer: (
                    <div style={{ textAlign: "center" }}>
                        <Button
                            className={isInCart ? "btn-danger" : "btn-success"}
                            func={() => {
                                cartHandler({
                                    articul,
                                    price,
                                    quantity: 1,
                                })
                                setModalContent(null)
                            }}
                            text={isInCart ? "Remove" : "Add"}
                        />
                        <Button
                            func={() => setModalContent(null)}
                            text={"Cancel"}
                        />
                    </div>
                ),
            })
        }
    }

    return [modalHandler]
}

export default useProductCartConfirm
