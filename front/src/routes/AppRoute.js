import React from "react"
import { Redirect, Route, Switch } from "react-router-dom"

import PageProducts from "../pages/PageProducts"
import PageCart from "../pages/PageCart"
import PageFavorites from "../pages/PageFavorites"

import Page404 from "../pages/Page404"

const AppRoute = () => {
    return (
        <div>
            <Switch>
                {/* моя идея, пока что */}
                <Redirect exact from="/" to="/products" />

                <Route exact path="/">
                    Главная страница
                </Route>
                <Route exact path="/products">
                    <PageProducts />
                </Route>
                <Route exact path="/cart">
                    <PageCart />
                </Route>
                <Route exact path="/favorites">
                    <PageFavorites />
                </Route>
                <Route path="*">
                    <Page404 />
                </Route>
            </Switch>
        </div>
    )
}

export default AppRoute
