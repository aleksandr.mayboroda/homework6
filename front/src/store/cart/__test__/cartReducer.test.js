import reducer from '../reducer'
import types from '../types'

const state = {
  data: []
}

describe('Cart Reducer Test', () => {
  const product = { 
    articul: 'aaa',
    price: 500,
    quantity: 1
  }

  test('Product adds to cart', () => {
    const action = {
      type: types.TOGGLE_CART,
      payload: product,
    }
    const newState = reducer(state, action)
    expect(newState.data).toContain(product)
  })

  test('Product remover from cart', () => {
    const action = {
      type: types.TOGGLE_CART,
      payload: product,
    }
    const newState = reducer(state, action)
    expect(newState.data).toContain(product)
  })

  test('Clear cart after submit order form', () => {
    let action = {
      type: types.TOGGLE_CART,
      payload: product,
    }
    const newState = reducer(state, action)
    expect(newState.data).toContain(product)

    action = {
      type: types.CLEAR_CART,
      payload: null,
    }
    const clearedState = reducer(newState, action)
    expect(clearedState.data).not.toContain(product)
  })
})