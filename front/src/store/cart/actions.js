import types from "./types"

const toggleCart = (productObject) => ({
    type: types.TOGGLE_CART,
    payload: productObject,
})

const clearCart = () => ({
    type: types.CLEAR_CART,
    // payload: []
})

const objForExport = { toggleCart, clearCart }

export default objForExport
