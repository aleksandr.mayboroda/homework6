import reducer from "../reducer"
import types from "../types"

const state = null

describe("Tests of modal reducer", () => {
    test("Set content to madal", () => {

        const content = "<div>Content</div>"

        const action = {
            type: types.SET_MODAL_CONTENT,
            payload: content,
        }

        const newState = reducer(null, action)
        expect(newState).toBe(content)
    })
})

