const getModalContent = () => state => state.modal

const defForExport = {getModalContent}

export default defForExport