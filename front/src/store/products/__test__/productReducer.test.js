import reducer from '../reducer'
import types from '../types'

const state = {
  data: [],
  favorites: [],
  error: null
}


describe('Test Products Reducer', () => {
  const products = [
    { 
    articul: 'bbb',
    price: 700,
    quantity: 1
    },
    { 
      articul: 'ccc',
      price: 1400,
      quantity: 1
    },
  ]

  test('Products array adds to store', () => {
    const action = {
      type: types.SET_PRODUCTS,
      payload: products,
    }
    const newState = reducer(state,action)
    expect(newState.data).toEqual(products)
  })

  test('Product adds to favorites', () => {
    const action = {
      type: types.TOGGLE_FAVORITES,
      payload: products[0].articul,
    }
    const newState = reducer(state,action)
    expect(newState.favorites).toContain(products[0].articul)
  })

  test('Product removes from favorites', () => {
    let action = {
      type: types.TOGGLE_FAVORITES,
      payload: products[1].articul,
    }
    let newState = reducer(state,action)
    action = {
      type: types.TOGGLE_FAVORITES,
      payload: products[1].articul,
    }
    newState = reducer(newState,action)
    expect(newState.favorites).not.toContain(products[1].articul)
  })

  test('Product store sets error', () => {
    const error = 'abcde'

    const action = {
      type: types.SET_ERROR,
      payload: error
    }

    const newState = reducer(state,action)
    expect (newState.error).toBe(error)
  })

  test('Product store error drops after products is set', () => {
    const error = 'fghij'

    let action = {
      type: types.SET_ERROR,
      payload: error
    }
    let newState = reducer(state,action)
    action = {
      type: types.SET_PRODUCTS,
      payload: products
    }
    newState = reducer(newState,action)
    expect(newState.error).toBe(null)
  })

})