import reducer from '../reducer'
import types from '../types'

const state = {
  data: [],
  orders: [],
  error: null
}


describe('Test Products Reducer', () => {
  const userData = {firstName: 'alex', lastName: 'maybee', age: 23}
  const error = 'error'
  const orders = [
    [{"articul":"gs1","price":500,"quantity":1}, {"articul":"ts1","price":250,"quantity":1}],
    [{"articul":"eee","price":3000,"quantity":1}]
  ]

  test('User data is set', () => {
    const action = {
      type: types.SET_USER,
      payload: userData
    }
    const newState = reducer(state,action)
    expect(newState.data).toEqual(userData)
  })

  test('Set error', () => {
    const action = {
      type: types.SET_ERROR,
      payload: error
    }
    const newState = reducer(state,action)
    expect(newState.error).toEqual(error)
  })

  test('Check error clear after data set', () => {
    let action = {
      type: types.SET_ERROR,
      payload: error
    }
    let newState = reducer(state,action)

    action = {
      type: types.SET_USER,
      payload: userData
    }
    newState = reducer(newState,action)
    expect(newState.error).toEqual(null)
  })

  test('Set Order', () => {
    let action = {
      type: types.SET_ORDER,
      payload: orders[0]
    }
    let newState = reducer(state,action)
    
    action.payload = orders[1]
    newState = reducer(newState,action)

    expect(newState.orders).toEqual(orders)
  })
})