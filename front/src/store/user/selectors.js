const getUserData = () => (state) => state.user.data
const getError = () => (state) => state.user.error

const getOrders = () => (state) => state.user.orders

const defForExport = {
    getUserData,
    getError,
    getOrders,
}

export default defForExport