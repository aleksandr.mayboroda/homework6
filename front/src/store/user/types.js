const SET_USER = 'homework/user/SET_USER'
const SET_ERROR = 'homework/user/SET_ERROR'

const SET_ORDER = 'homework/user/SET_ORDER'

const defForExport = {SET_USER,SET_ERROR,SET_ORDER}

export default defForExport